# dependency
!CTL_CORE_MODULE: error("REGIST_2D3D_MODULE needs CTL_CORE_MODULE -> include ctl_core.pri before 2d3d_regist.pri")
!OCL_CONFIG_MODULE: error("REGIST_2D3D_MODULE needs OCL_CONFIG_MODULE -> include ocl_config.pri before 2d3d_regist.pri")
!OCL_ROUTINES_MODULE: error("REGIST_2D3D_MODULE needs OCL_ROUTINES_MODULE -> include ocl_routines.pri before 2d3d_regist.pri")

# declare module
CONFIG += REGIST_2D3D_MODULE
DEFINES += REGIST_2D3D_MODULE_AVAILABLE

HEADERS += \
    $$PWD/../src/registration/grangeatregistration2d3d.h \
    $$PWD/../src/registration/projectionregistration2d3d.h

SOURCES += \
    $$PWD/../src/registration/grangeatregistration2d3d.cpp \
    $$PWD/../src/registration/projectionregistration2d3d.cpp

# NLopt library [ https://github.com/stevengj/nlopt/ ]
unix:LIBS += -L/usr/local/lib
LIBS += -lnlopt
