#include "errormetrics.h"
#include "img/chunk2d.h"
#include "img/voxelvolume.h"
#include <stdexcept>
#include <typeinfo>

namespace CTL {

namespace metric {
    const ::CTL::imgproc::L1Norm L1;
    const ::CTL::imgproc::RelativeL1Norm rL1;
    const ::CTL::imgproc::L2Norm L2;
    const ::CTL::imgproc::RelativeL2Norm rL2;
    const ::CTL::imgproc::RMSE RMSE;
    const ::CTL::imgproc::RelativeRMSE rRMSE;
    const ::CTL::imgproc::CorrelationError corrErr;
    const ::CTL::imgproc::CosineSimilarityError cosSimErr;
    const ::CTL::imgproc::GemanMcClure GMCPreuhs(0.25);
    const ::CTL::imgproc::NormalizedGemanMcClure nGMCPreuhs(0.25);
} // namespace metric

namespace imgproc {

double AbstractErrorMetric::operator()(const std::vector<float>& vec1,
                                       const std::vector<float>& vec2) const
{
    if(vec1.size() != vec2.size())
    {
        std::string error = typeid(*this).name();
        error.append(" -> AbstractErrorMetric::operator("
                     "const std::vector<float>& vec1, "
                     "const std::vector<float>& vec2): "
                     "Vectors must have the same length.");
        throw std::domain_error(error);
    }

    return (*this)(vec1.data(), vec1.data() + vec1.size(), vec2.data());
}

double AbstractErrorMetric::operator()(const Chunk2D<float>& chunk1,
                                       const Chunk2D<float>& chunk2) const
{
    if(!chunk1.hasData())
        qWarning("`chunk1` has no data.");
    if(!chunk2.hasData())
        qWarning("`chunk2` has no data.");

    if(chunk1.dimensions() != chunk2.dimensions())
    {
        std::string error = typeid(*this).name();
        error.append(" -> AbstractErrorMetric::operator("
                     "const Chunk2D<float>& chunk1, "
                     "const Chunk2D<float>& chunk2): "
                     "Chunks must have the same dimensions.");
        throw std::domain_error(error);
    }

    return (*this)(chunk1.data(), chunk2.data());
}

double AbstractErrorMetric::operator()(const VoxelVolume<float>& vol1,
                                       const VoxelVolume<float>& vol2) const
{
    if(!vol1.hasData())
        qWarning("`vol1` has no data.");
    if(!vol2.hasData())
        qWarning("`vol2` has no data.");

    if(vol1.dimensions() != vol2.dimensions())
    {
        std::string error = typeid(*this).name();
        error.append(" -> AbstractErrorMetric::operator("
                     "const VoxelVolume<float>& vol1, "
                     "const VoxelVolume<float>& vol2): "
                     "VoxelVolumes must have the same dimensions.");
        throw std::domain_error(error);
    }

    return (*this)(vol1.data(), vol2.data());
}

double L1Norm::operator()(const float* first1, const float* last1, const float* first2) const
{
    return this->operator()<const float*, const float*>(first1, last1, first2);
}

double RelativeL1Norm::operator()(const float* first1, const float* last1, const float* first2) const
{
    return this->operator()<const float*, const float*>(first1, last1, first2);
}

double L2Norm::operator()(const float* first1, const float* last1, const float* first2) const
{
    return this->operator()<const float*, const float*>(first1, last1, first2);
}

double RelativeL2Norm::operator()(const float* first1, const float* last1, const float* first2) const
{
    return this->operator()<const float*, const float*>(first1, last1, first2);
}

double RMSE::operator()(const float* first1, const float* last1, const float* first2) const
{
    return this->operator()<const float*, const float*>(first1, last1, first2);
}

double RelativeRMSE::operator()(const float* first1, const float* last1, const float* first2) const
{
    return this->operator()<const float*, const float*>(first1, last1, first2);
}

double CorrelationError::operator()(const float* first1, const float* last1, const float* first2) const
{
    return this->operator()<const float*, const float*>(first1, last1, first2);
}

double CosineSimilarityError::operator()(const float* first1, const float* last1, const float* first2) const
{
    return this->operator()<const float*, const float*>(first1, last1, first2);
}

double GemanMcClure::operator()(const float* first1, const float* last1, const float* first2) const
{
    return this->operator()<const float*, const float*>(first1, last1, first2);
}

GemanMcClure::GemanMcClure(const double parameter)
    : _parameter(parameter)
{
    if( !(parameter > 0.0) )
        throw std::domain_error("GemanMcClure parameter must be greater than zero.");
}

double GemanMcClure::parameter() const { return _parameter; }

double NormalizedGemanMcClure::operator()(const float* first1, const float* last1, const float* first2) const
{
    return this->operator()<const float*, const float*>(first1, last1, first2);
}

NormalizedGemanMcClure::NormalizedGemanMcClure(double parameter)
    : _parameter(parameter)
{
    if( !(parameter > 0.0) )
        throw std::domain_error("RelativeGemanMcClure parameter must be greater than zero.");
}

double NormalizedGemanMcClure::parameter() const { return _parameter; }

} // namespace imgproc
} // namespace CTL
